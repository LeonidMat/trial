package org.itstep.qa.lesson;


public class Rectangle {
    private int x1;
    private int y1;
    private int x2;
    private int y2;

    Rectangle(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Rectangle(int height, int width) {
        this.x1 = 0;
        this.y1 = 0;
        this.x2 = height;
        this.y2 = width;
    }

    public Rectangle() {
        this.x1 = 0;
        this.y1 = 0;
        this.x2 = 0;
        this.y2 = 0;
    }
}

